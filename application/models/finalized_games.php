<?php
Class Finalized_games extends CI_Model{

    function register($user_id,$time,$difficulty){
        $data = array(
            'user_id' => $user_id ,
            'time' => $time,
            'difficulty' => $difficulty
        );
        $this->db->insert('finalized_games', $data);

    }

    function get_rank($difficulty){
        $this->db->select('u.username, fg.`time`');
        $this->db->join('finalized_games fg','u.id = fg.user_id');
        $this->db->where('fg.difficulty',$difficulty);
        $this->db->order_by('fg.`time`', 'asc');
        $this->db->limit(10);
        return $this -> db -> get('user u')->result();
    }

}
?>