<?php
Class User extends CI_Model{
    function login($username, $password){
        $this -> db -> select('id, username, password');
        $this -> db -> from('user');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', sha1($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }

    function register($username,$password){
        $data = array(
            'username' => $username ,
            'password' => sha1($password)
        );
        if($this->is_username_available($username)) {
            $this->db->insert('user', $data);
            return true;
        }else{
            return false;
        }
    }

    function is_username_available($username){
        $this -> db -> select('username');
        $this -> db -> from('user');
        $this -> db -> where('username', $username);
        $query = $this -> db -> get();

        if($query -> num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }


}
?>