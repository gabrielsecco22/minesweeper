<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/other/game.css'); ?>"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/js/game.js'); ?>"></script>
    <link rel="shortcut icon" type="image" href="<?php echo base_url('assets/img/favicon.ico'); ?>"/>
    <title>Minesweeper</title>
</head>

<?php
    //MAP DIMENSIONS
    $width = $map['width'];
    $height = $map['height'];
    $num_mines = $map['num_mines'];
    //$num_mines =1;
?>

<BODY onload = init(<?php echo $width.",".$height.",".$num_mines.",\"".$difficulty."\"" ?>)>
    <DIV CLASS=contents>
        <nav>
            <ul>
                <li><a class="<?php echo $difficulty == "beginner"?'active':''; ?>"
                       href="<?php echo site_url('home/?difficulty=beginner'); ?>">Beginner</a></li>
                <li><a class="<?php echo $difficulty == "intermediate"?'active':''; ?>"
                       href="<?php echo site_url('home/?difficulty=intermediate'); ?>">Intermediate</a></li>
                <li><a class="<?php echo $difficulty == "expert"?'active':''; ?>"
                       href="<?php echo site_url('home/?difficulty=expert'); ?>">Expert</a></li>
                <!-- <li><a class="active" href="#about">About</a></li> -->
                <ul style="float:right;list-style-type:none;">
                    <li><a href="<?php echo base_url('index.php/home/logout'); ?>">Logout</a></li>
                </ul>
            </ul>
        </nav>

        <TABLE CLASS=sq ID=sqTable oncontextmenu="return false">
            <TR>
                <TD CLASS=score COLSPAN=<?php echo $width ?>>
                    <DIV CLASS=counter ID=mines><?php echo $num_mines; ?></DIV>
                    <DIV CLASS=counter ID=timer>000</DIV>
                    <IMG ID=smiley SRC="<?php echo base_url('assets/img/bored.gif'); ?>" ALT=""
                     onmousedown="return clickSmiley(event)">
                </TD>
            </TR>

            <!-- MAP CONTENTS -->
            <?php
                for($i = 0; $i<$height; $i++){
                    echo '<TR>'."\n";
                    for($j = 0; $j<$width; $j++){
                        $id = $width * $i + $j;
                        echo "<TD CLASS=sq ID=\"sq-$id\"
                            onmousedown=\"return clickSq(event,$id)\"
                            >&nbsp;</TD>"."\n";
                    }
                    echo "\n".'</TR>';
                }

            ?>
        </TABLE>

        <div id = table_container>
            <TABLE class="rank">
                <TR class="rank">
                    <TH class="rank-diff" style="width:15%;"></TH>
                    <TH class="rank-diff" style="width:70%;">Beginner</TH>
                    <TH class="rank-diff" style="width:15%;"></TH>
                </TR>
                <TR class="rank">
                    <TH class="rank" style="width:15%;">Rank</TH>
                    <TH class="rank" style="width:70%;">User</TH>
                    <TH class="rank" style="width:15%;">Time(s)</TH>
                </TR>
                <?php $i = 1;
                foreach($rank_table_beginner as $line) {
                    echo "<TR class=rank >
                    <TD class=rank > $i </TD >
                    <TD class=rank > $line->username </TD >
                    <TD class=rank > $line->time </TD >
                </TR >";
                    $i++;
                }
                ?>
            </TABLE>

            <TABLE class="rank">
                <TR class="rank">
                    <TH class="rank-diff" style="width:15%;"></TH>
                    <TH class="rank-diff" style="width:70%;">Intermediate</TH>
                    <TH class="rank-diff" style="width:15%;"></TH>
                </TR>
                <TR class="rank">
                    <TH class="rank" style="width:15%;">Rank</TH>
                    <TH class="rank" style="width:70%;">User</TH>
                    <TH class="rank" style="width:15%;">Time(s)</TH>
                </TR>
                <?php $i = 1;
                foreach($rank_table_intermediate as $line) {
                    echo "<TR class=rank >
                    <TD class=rank > $i </TD >
                    <TD class=rank > $line->username </TD >
                    <TD class=rank > $line->time </TD >
                </TR >";
                    $i++;
                }
                ?>
            </TABLE>

            <TABLE class="rank">
                <TR class="rank">
                    <TH class="rank-diff" style="width:15%;"></TH>
                    <TH class="rank-diff" style="width:70%;">Expert</TH>
                    <TH class="rank-diff" style="width:15%;"></TH>
                </TR>
                <TR class="rank">
                    <TH class="rank" style="width:15%;">Rank</TH>
                    <TH class="rank" style="width:70%;">User</TH>
                    <TH class="rank" style="width:15%;">Time(s)</TH>
                </TR>
                <?php $i = 1;
                foreach($rank_table_expert as $line) {
                    echo "<TR class=rank >
                    <TD class=rank > $i </TD >
                    <TD class=rank > $line->username </TD >
                    <TD class=rank > $line->time </TD >
                </TR >";
                    $i++;
                }
                ?>
            </TABLE>
        </div>
    </DIV>

</body>

</html>