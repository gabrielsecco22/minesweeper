<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
       <!-- Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/other/login.css'); ?>"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/js/login.js'); ?>"></script>
    <link rel="shortcut icon" type="image" href="<?php echo base_url('assets/img/favicon.ico'); ?>"/>



</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="#" class="active" id="login-form-link">Login</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" id="register-form-link">Register</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="login-form" action="<?php echo base_url('verify_login/login'); ?>" method="post" role="form" style="display: block;">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            <!-- type="submit" -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <form id="register-form" action="<?php echo base_url('register'); ?>"  method="post" role="form" style="display: none;">
                                    <div class="form-group">
                                        <input type="text" name="username2" id="username2" tabindex="1" class="form-control" placeholder="Username" value=""
                                               onBlur=checkUserNameAvailability("<?php echo base_url('register/check_new_username'); ?>")>
                                        <span id="user-availability-status"></span>
                                    </div>
                                    <p><img src="<?php echo base_url('assets/img/LoaderIcon.gif'); ?>" id="loaderIcon" style="display:none" /></p>
                                    <div class="form-group">
                                        <input type="password" name="password2" id="password2" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


</html>