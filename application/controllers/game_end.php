<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_end extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('finalized_games','',TRUE);
    }

    function register(){
        // sending info to login.js
        if(!empty($_POST["time"]) && !empty($_POST["dif"])) {
            $time = $_POST["time"];
            $difficulty = $_POST["dif"];
            $session_data = $this->session->userdata('logged_in');
            $user_id = $session_data['id'];

            $this->finalized_games->register($user_id,$time,$difficulty);
        }
    }
}
?>