<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('user','',TRUE);
    }

    function index(){
        //This method will have the credentials validation
        $response = false;

        if(!empty($_POST["username2"]) && !empty($_POST["password2"])) {
            $username = $_POST["username2"];
            $password = $_POST["password2"];
            if(strlen($username) >= 4) {
                $response = $this->user->register($username, $password);
            }else{
                echo "<ul class=notice_errors>
                    <li>Username must have at least 4 characters</li>
                </ul>";
            }
        }else if(empty($_POST["username2"])){
            echo "<ul class=notice_errors>
                <li>Username field is required</li>
            </ul>";
        }else if(empty($_POST["password2"])){
            echo "<ul class=notice_errors>
                <li>Password field is required</li>
            </ul>";
        }

        if($response == true) {
            echo '<ul class=notice_errors style="background-color:#00AA00;">
                <li>Registration was successful! You now can login and play!</li>
            </ul>';
            //redirect('home', 'refresh');
        }
        $this->load->view('login_view');
    }

    function check_new_username(){
        // sending info to login.js
        if(!empty($_POST["username"])) {
            $username = $_POST["username"];
            if($this->user->is_username_available($username) == false) {
                echo 0;
                //return false;
                //echo "<span class='status-not-available'> Username Not Available.</span>";
            }else {
                echo 1;
                //return true;
                //echo "<span class='status-available'> Username Available.</span>";
            }
        }
    }
}
?>