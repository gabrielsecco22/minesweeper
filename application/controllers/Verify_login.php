<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verify_login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function login(){
   //This method will have the credentials validation
     $response = false;

     if(!empty($_POST["username"]) && !empty($_POST["password"])) {
         $username = $_POST["username"];
         $password = $_POST["password"];
         $response = $this->check_database($username, $password);
     }

    if($response == false) {
       //Field validation failed.  User redirected to login page
        echo "<ul class=notice_errors>
                <li>Invalid username or password</li>
            </ul>";
        $this->load->view('login_view');

    }else {
       //Go to private area
       redirect('home', 'refresh');
    }
 }

    function check_database($username, $password){
        //query the database
        $result = $this->user->login($username, $password);
        if($result) {
            $sess_array = array();
            foreach($result as $row) {
                $sess_array = array(
                   'id' => $row->id,
                   'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return true;
        } else {
         return false;
        }
    }
}
?>