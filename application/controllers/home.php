<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('finalized_games','',TRUE);
        $this->load->helper('url');
    }

    function index(){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $difficulties = array("beginner","intermediate","expert");
            $default_difficulty = "intermediate";
            $difficulty = $this->input->get('difficulty');
            if(!$difficulty || !in_array($difficulty,$difficulties)){
                $difficulty = $default_difficulty;
            }
            $map = array(
                "beginner" => array(
                    "width" => 9,
                    "height" => 9,
                    "num_mines" => 10
                ),
                "intermediate" => array(
                    "width" => 16,
                    "height" => 16,
                    "num_mines" => 40
                ),
                "expert" => array(
                    "width" => 30,
                    "height" => 16,
                    "num_mines" => 99
                )
            );
            $data['map'] = $map[$difficulty];
            $data['difficulty'] = $difficulty;
            $data['username'] = $session_data['username'];
            $data['rank_table_beginner'] = $this->finalized_games->get_rank("beginner");
            $data['rank_table_intermediate'] = $this->finalized_games->get_rank("intermediate");
            $data['rank_table_expert'] = $this->finalized_games->get_rank("expert");
           // var_dump($difficulty);die;
            $this->load->view('home_view', $data);
        }
        else {
           //If no session, redirect to login page
           redirect('login', 'refresh');
        }
    }

    function logout(){
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }

}

?>