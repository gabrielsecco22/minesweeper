/**
 * Created by Gabriel on 24/02/2016.
 */


//timer status and variables
var timer; // specifies if timer is counting or should be incremented
var startTime; // game start time


//Game status variables
var playing = 0;                    // smiley absent during initialization
var sad = 1;                        // smiley value after loss
var bored = 2;                      // smiley value during game
var happy = 3;                      // smiley value after win
var sadness = bored;
//Map variables
var revealed = []; // for auto reveal algorithm
var cleared = []; //squares (not mines) revealed - used for winning checks
var flagged = []; //flags
var map = []; // minesweeper's map
var numOfMines;
var width;              // map's width
var height;             // map's height
var difficulty;         // map's difficulty
var mine = "\uD83D\uDCA3"; // UTF-8 Char for BOMB
var charInfinity = "&#x221E;";
var charFlag = "!";
var sqColor = [ // Number colors
    "#000000", "#3333cc", "#006600",
    "#cc0000", "#0066CC", "#A52A2A",
    "#00cccc", "#000000", "#666666"];

function clickSq(event, thisSquare) {
    if (sadness != bored) return false; // Game over: do nothing
    if (!timer) startTimer();

    //TODO: right + left click

    if (event.button == 2) {
        // only works on non-cleared squares
        if(cleared.indexOf(thisSquare) == -1){
            if(flagged.indexOf(thisSquare) != -1){
                removeFlag(thisSquare);
            }else{
                putFlag(thisSquare);
            }
        }
    } else {
        //start timer if not already set

        // Perform click Action
        //remove flag before cause it also changes the innerHtml so it'll be override
        removeFlag(thisSquare);
        var sq = document.getElementById("sq-" + thisSquare);
        sq.innerHTML = map[thisSquare];
        sq.style.backgroundColor = "#bbbbbb";
        sq.style.borderColor = "#bbbbbb";
        sq.style.color = clickColor(thisSquare);
        // sq.style.cursor = "default";
        pushToCleared(thisSquare);


        // Check if clicked position is a mine
        if (map[thisSquare] == mine) {
            revealAllMines();
            sadness = sad;
            timer = false;
        } else if (map[thisSquare] == "&nbsp;") {
            revealRecursive(thisSquare);
        }

        //win condition
        if (cleared.length == (width * height - numOfMines)) {
            sadness = happy;
            //send timer to database for insert
            timer = false;
            saveWin();
        }
        setSmiley();
    }
}

//function for changing square font color depending on numer of adjacent mines
function clickColor(thisSquare){
    var c = "#000000";
    if(map[thisSquare] == mine){
        c = "#ff0000";
    }else if(map[thisSquare] != "&nbsp;"){
        c = sqColor[map[thisSquare]];
    }
    return c;
}

function init(w,h,numMines,difficult){
    //setting game status variables
    sadness = bored;
    setSmiley();

    //setting map variables
    revealed = [];
    cleared = [];
    flagged = [];
    map = [];
    numOfMines = numMines;
    width = w;
    height = h;
    difficulty = difficult;

    //setting timer and remaing mines variables
    timer = false;
    var elt = document.getElementById("timer");
    elt.innerHTML = "000";
    var rmines = document.getElementById("mines");
    rmines.innerHTML = numOfMines;

    //create an array containing mines positions
    var arr = [];
    while(arr.length < numMines){
        var randomnumber=Math.ceil(Math.random()*(width * height));
        var found=false;
        for(var i=0;i<arr.length;i++){
            if(arr[i]==randomnumber){
                found=true;
                break;
            }
        }
        if(!found)
            arr[arr.length]=randomnumber;
    }
    //putting mines into map
    for(i = 0; i < width * height; i++ ){
        map[i] = 0;
        var sq = document.getElementById("sq-" + i);
        sq.innerHTML = "&nbsp;";
        sq.style.backgroundColor = "#cccccc";
        sq.style.borderColor = "#eeeeee #999999 #999999 #eeeeee";
        sq.style.color = "#000000";
        //sq.hover("#000000");
        // sq.style.cursor = "pointer";
    }
    for(i = 0; i < numMines; i++){
        map[arr[i]] = mine;
    }
    //Numbers for non mined positions
    for(i = 0; i < width * height; i++ ){
        if(map[i] == mine){
            applyToNeighbours(i,neighbourIsMine);
        }
    }
    //After that, get ones wich are zeroes and trade for spaces
    for(i = 0; i < width * height; i++ ){
        if(map[i] == 0) map[i] = "&nbsp;";
    }

}

function neighbourIsMine(thisSquare) {
    // Increase adjacency count, if this isn't itself a mine
    if (map[thisSquare] != mine) map[thisSquare]++;
}

//function triggered when you loose the game
function revealAllMines(){
    for(i = 0; i < width * height; i++ ){
        if(map[i] == mine){
            var sq = document.getElementById("sq-" + i);
            sq.innerHTML = map[i];
            sq.style.backgroundColor = "#bbbbbb";
            sq.style.borderColor = "#bbbbbb";
            // sq.style.cursor = "default";
        }
    }
}

function revealRecursive(thisSquare){
    revealed.push(thisSquare);
    applyToNeighbours(thisSquare,clearSq);
}

function clearSq(thisSquare){
    //remove flag before cause it also changes the innerHtml so it'll be override
    removeFlag(thisSquare);
    var sq = document.getElementById("sq-" + thisSquare);
    sq.innerHTML = map[thisSquare];
    sq.style.backgroundColor = "#bbbbbb";
    sq.style.borderColor = "#bbbbbb";
    sq.style.color = clickColor(thisSquare);
    // sq.style.cursor = "default";
    pushToCleared(thisSquare);
    //if the square is flagged , the function will de-flag it;

    if(map[thisSquare] == "&nbsp;" && revealed.indexOf(thisSquare) == -1){
        revealRecursive(thisSquare);
    }
}

function applyToNeighbours(thisSquare, f) {
    // Apply given function to each existing neighbours of given square
    // This is the only part of the program that knows the topology
    // The performance of this function has a visible effect on the program
    var total = width * height;
    var x = thisSquare % width;
    if (thisSquare >= width) { // there's a row above
        if (x > 0) f(thisSquare - width - 1);
        f(thisSquare - width);
        if (x+1 < width) f(thisSquare - width + 1);
    }
    if (x > 0) f(thisSquare - 1);
    if (x+1 < width) f(thisSquare + 1);
    if (thisSquare < total-width) { // there's a row below
        if (x > 0) f(thisSquare + width - 1);
        f(thisSquare + width);
        if (x+1 < width) f(thisSquare + width + 1);
    }
}

function pushToCleared(thisSquare){
    if(cleared.indexOf(thisSquare) == -1 && map[thisSquare] != mine) cleared.push(thisSquare);
}

function clickSmiley(event){
    if (event.button == 0) {
        init(width, height, numOfMines,difficulty);
    }
}

function setSmiley() {
    // update the happy/sad icon display
    var imgFolder =window.location.origin+"/minesweeper/assets/img";
    var smiley = document.getElementById("smiley");
    smiley.src = (sadness == sad ? imgFolder+"/sad.gif" :
        (sadness == bored ? imgFolder+"/bored.gif" :
        imgFolder+"/happy.gif"));
}




// -------------------------------------------TIMER AND FLAG functions ---------------------------------------------------
function setElapsed() {
    // update elapsed time display
    var elt = document.getElementById("timer");
    if (timer) {
        var now = new Date();
        var secs = Math.floor((now.getTime() - startTime.getTime())/1000);
        elt.innerHTML = (secs > 999 ? charInfinity : "" + secs);
    } else {
        elt.innerHTML = "000";
    }
}

function timerAction() {
    // Called via setTimeout
    // Update the elapsed time, and schedule another call if wanted
    // Note: setInterval is similar, but stops (Safarai 1.3) after
    // user has navigated away then returned to the page.
    if (timer) {
        setElapsed();
        setTimeout("timerAction()", 100);
    }
}

function startTimer() {
    startTime = new Date();
    timer = true;
    timerAction();
}

function putFlag(thisSquare){
    if(numOfMines - flagged.length > 0){
        //put the flag
        flagged.push(thisSquare);
        var sq = document.getElementById("sq-" + thisSquare);
        sq.innerHTML = charFlag;
        //update remaining mines
        var rmines = document.getElementById("mines");
        rmines.innerHTML = numOfMines - flagged.length;
    }
}

function removeFlag(thisSquare){
    if(flagged.indexOf(thisSquare) != -1) {
        flagged.splice(flagged.indexOf(thisSquare), 1);

        var sq = document.getElementById("sq-" + thisSquare);
        sq.innerHTML = "&nbsp;";

        var rmines = document.getElementById("mines");
        rmines.innerHTML = numOfMines - flagged.length;
    }

}

// -------------------------------------------DATABASE functions ---------------------------------------------------
function saveWin() {
    var now = new Date();
    var secs = Math.floor((now.getTime() - startTime.getTime())/10)/100;
    alert("Congrats, you won " + difficulty + " difficulty in " + secs + " seconds! - Refresh page for rank updates");
    jQuery.ajax({
            url: window.location.origin+"/minesweeper/game_end/register",
        //data:'time=' + secs,
        data: { time: secs, dif: difficulty },
    type: "POST",
        success:function(data){
        //TODO AJAX Update rank list
    },
    error:function (){}
});
}
