/**
 * Created by Gabriel on 17/02/2016.
 */

$(function() {
    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
});

function checkPassword(){
    var pass1 = document.getElementById("password2").value;
    var pass2 = document.getElementById("confirm-password").value;
    var ok = true;

    var passDefaulBorder = document.getElementById("password2").style.borderColor;
    var confDefaulBorder = document.getElementById("confirm-password").style.borderColor;
    var confStatusDefaultHtml = document.getElementById("confirm-password-status").innerHTML;

    if (pass1 != pass2) {
        //alert("Passwords Do not match " + pass1 +" != "+ pass2);
        document.getElementById("password2").style.borderColor = "#E34234";
        document.getElementById("confirm-password").style.borderColor = "#E34234";
        document.getElementById("confirm-password-status").innerHTML = "Password confirmation doesn't match.";
        ok = false;
    }
    else {
        document.getElementById("password2").style.borderColor = "#ddd";
        document.getElementById("confirm-password").style.borderColor = "#ddd";
        document.getElementById("confirm-password-status").innerHTML = "Password confirmation OK";
        //alert("Passwords DO match " + pass1 +" == "+ pass2);
    }
    return ok;
}

function checkUserNameAvailability(path) {

    $("#loaderIcon").show();
    var val = $("#username2").val();
    if(val.length <= 3){
        html_data = "<span class='status-not-available'> Username must have at least 4 characters</span>";
        $("#register-submit").attr("disabled", true);
        $("#user-availability-status").html(html_data);
        $("#loaderIcon").hide();
    }else{
        jQuery.ajax({
            //url: "check_availability.php",
            url : path,
            data: 'username=' + val,
            type: "POST",
            success:function(data){

                // data = 0 - username not available | data =1 - username available
                var html_data;
                if(data == 0){
                    html_data = "<span class='status-not-available'> Username Not Available.</span>";
                    $("#register-submit").attr("disabled", true);
                }else{ //data ==1
                    html_data = "<span class='status-available'> Username Available.</span>";
                    $("#register-submit").removeAttr("disabled");
                }
                $("#user-availability-status").html(html_data);
                $("#loaderIcon").hide();
                if(data == 0) return false;
                else return true;
            },
            error:function (){
                return false;
            }
        });
    }
}